package pl.aprilapps.library.utils;

public class GAEResponseObject<T> {

	private int statusCode = 200;
	private Object object;
	private String nextPageCursor;
	private boolean clearList;

	public GAEResponseObject(int statusCode, Object object) {
		super();
		this.statusCode = statusCode;
		this.object = object;
	}

	public GAEResponseObject(Object object, String nextPageCursor) {
		super();
		this.object = object;
		this.nextPageCursor = nextPageCursor;
	}

	public GAEResponseObject(Object object) {
		super();
		this.object = object;
	}

	public GAEResponseObject(int statusCode) {
		super();
		this.statusCode = statusCode;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getNextPageCursor() {
		return nextPageCursor;
	}

	@SuppressWarnings("unchecked")
	public T getObject() {
		return (T) object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public boolean isOk() {
		return getStatusCode() == 200;
	}

	public boolean isClearList() {
		return clearList;
	}

	public void setClearList(boolean clearList) {
		this.clearList = clearList;
	}

}
