package pl.aprilapps.library.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class AaImageUtils extends AaUtils {

    public static Bitmap loadBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            Log.w("AA", "Can't load image for URL: " + src);
            return null;
        }
    }

    public static BitmapDrawable loadDrawableFromURL(Context context, String src) {
        try {
            return new BitmapDrawable(context.getResources(), loadBitmapFromURL(src));
        } catch (Exception e) {
            return null;
        }

    }

    public static Bitmap loadGooglePlusAvatarFromURL(String urlString, int size) {
        try {
            urlString = urlString.replaceAll("sz=\\d+", ("sz=" + size));
            return loadBitmapFromURL(urlString);
        } catch (Exception e) {
            return null;
        }
    }


}
