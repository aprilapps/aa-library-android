package pl.aprilapps.library.utils;

import java.text.NumberFormat;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AaCurrencyFormatter {

	public enum SymbolPosition {
		Left, Right;
	}

	SharedPreferences preferences;

	public static final String KEY_SYMBOL_POSITION = "symbol_position_preference";
	public static final String KEY_SYMBOL = "symbol_preference";

	// public static final String DEFAULT_CURRENCY_SYMBOL = "&#8364;";
	public static final String DEFAULT_CURRENCY_SYMBOL = "$";

	private NumberFormat numberFormat;
	private Context context;

	public AaCurrencyFormatter(Context context) {
		this.context = context;
		this.numberFormat = NumberFormat.getInstance();
		this.numberFormat.setMinimumFractionDigits(2);
		this.numberFormat.setMaximumFractionDigits(2);
		preferences = PreferenceManager.getDefaultSharedPreferences(context);
	}

	public String format(double number) {
		String numberString = numberFormat.format(number);
		String symbolString = preferences.getString(KEY_SYMBOL, DEFAULT_CURRENCY_SYMBOL);

		String symbolPosition = preferences.getString(KEY_SYMBOL_POSITION, SymbolPosition.Left.toString());
		SymbolPosition position = SymbolPosition.valueOf(symbolPosition);

		switch (position) {
		case Left:
			return symbolString + " " + numberString;
		case Right:
			return numberString + " " + symbolString;
		default:
			return symbolString + " " + numberString;
		}
	}

	public String format(float number) {
		String numberString = numberFormat.format(number);
		String symbolString = preferences.getString(KEY_SYMBOL, DEFAULT_CURRENCY_SYMBOL);

		String symbolPosition = preferences.getString(KEY_SYMBOL_POSITION, SymbolPosition.Left.toString());
		SymbolPosition position = SymbolPosition.valueOf(symbolPosition);

		switch (position) {
		case Left:
			return symbolString + " " + numberString;
		case Right:
			return numberString + " " + symbolString;
		default:
			return symbolString + " " + numberString;
		}
	}

	public String format(int number) {
		String numberString = numberFormat.format(number);
		String symbolString = preferences.getString(KEY_SYMBOL, DEFAULT_CURRENCY_SYMBOL);

		String symbolPosition = preferences.getString(KEY_SYMBOL_POSITION, SymbolPosition.Left.toString());
		SymbolPosition position = SymbolPosition.valueOf(symbolPosition);

		switch (position) {
		case Left:
			return symbolString + " " + numberString;
		case Right:
			return numberString + " " + symbolString;
		default:
			return symbolString + " " + numberString;
		}
	}

	public String format(double number, boolean addCurrencySymbol) {
		String numberString = numberFormat.format(number);
		String symbolString = preferences.getString(KEY_SYMBOL, DEFAULT_CURRENCY_SYMBOL);

		String symbolPosition = preferences.getString(KEY_SYMBOL_POSITION, SymbolPosition.Left.toString());
		SymbolPosition position = SymbolPosition.valueOf(symbolPosition);

		if (addCurrencySymbol) {
			switch (position) {
			case Left:
				return symbolString + " " + numberString;
			case Right:
				return numberString + " " + symbolString;
			default:
				return symbolString + " " + numberString;
			}
		} else {
			return numberString;
		}
	}

	public String getExchangeRate(float number) {
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(4);
		nf.setMaximumFractionDigits(4);

		String numberString = nf.format(number);
		return numberString;
	}

	public String getExchangeRate(double number) {
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(4);
		nf.setMaximumFractionDigits(4);

		String numberString = nf.format(number);
		return numberString;
	}

	public String getExchangeRateWithCurrency(float number) {
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(4);
		nf.setMaximumFractionDigits(4);
		String numberString = nf.format(number);
		String symbolString = preferences.getString(KEY_SYMBOL, DEFAULT_CURRENCY_SYMBOL);

		String symbolPosition = preferences.getString(KEY_SYMBOL_POSITION, SymbolPosition.Left.toString());
		SymbolPosition position = SymbolPosition.valueOf(symbolPosition);

		switch (position) {
		case Left:
			return symbolString + " " + numberString;
		case Right:
			return numberString + " " + symbolString;
		default:
			return symbolString + " " + numberString;
		}
	}

	public boolean isSymbolOnTheLeft() {
		String symbolPosition = preferences.getString(KEY_SYMBOL_POSITION, SymbolPosition.Left.toString());
		SymbolPosition position = SymbolPosition.valueOf(symbolPosition);
		return position == SymbolPosition.Left;
	}

	public String getCurrencySymbol() {
		return preferences.getString(KEY_SYMBOL, DEFAULT_CURRENCY_SYMBOL);
	}

	public void setCurrencySymbolPositionOnTheLeft() {
		preferences.edit().putString(KEY_SYMBOL_POSITION, SymbolPosition.Left.toString()).commit();
	}

	public void setCurrencySymbolPositionOnTheRight() {
		preferences.edit().putString(KEY_SYMBOL_POSITION, SymbolPosition.Right.toString()).commit();
	}

	public void setCurrencySymbol(String currencySymbol) {
		preferences.edit().putString(KEY_SYMBOL, currencySymbol).commit();
	}
}
