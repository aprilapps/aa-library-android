package pl.aprilapps.library.utils.communication;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import android.content.Context;
import android.util.Log;

public abstract class AaPostRequest<T> extends AaRequest<T> {

	private StringBuilder paramsBuilder;
	private String json;

	public AaPostRequest(String functionName, Context context) throws MalformedURLException {
		super(functionName, context);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void buildQuery() throws IOException, UnsupportedEncodingException {
		if (params.size() > 0) {
			paramsBuilder = new StringBuilder();
			Iterator it = params.entrySet().iterator();
			int i = 0;
			while (it.hasNext()) {
				Map.Entry<String, Object> pair = (Map.Entry) it.next();
				Object value = pair.getValue();

				String param = value.toString();
				String key = pair.getKey();

				if (i == 0) {
					paramsBuilder.append(key).append("=").append(URLEncoder.encode(param, "utf-8"));
				} else {
					paramsBuilder.append("&").append(key).append("=").append(URLEncoder.encode(param, "utf-8"));
				}
				i++;
			}
		}
	}

	@Override
	public InputStream execute() throws Exception {

		buildQuery();
		url = new URL(urlBuilder.toString());
		if (useSSL()) {
			connection = (HttpsURLConnection) url.openConnection();
		} else {
			connection = (HttpURLConnection) url.openConnection();
		}
		connection.setConnectTimeout(getTimeout());
		injectHeders();

		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		if (json != null) {
			connection.setRequestProperty("Content-Type", "application/json; charset=utf8");
			connection.setRequestProperty("Accept", "application/json");

			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(json);
			wr.flush();
			wr.close();

		} else if (paramsBuilder != null) {
			connection.setRequestProperty("Content-Length", "" + Integer.toString(paramsBuilder.toString().getBytes("UTF-8").length));

			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(paramsBuilder.toString());
			wr.flush();
			wr.close();
		}

		statusCode = connection.getResponseCode();

		if (statusCode >= 200 && statusCode < 300) {
			InputStream responseData = connection.getInputStream();
			return responseData;
		} else {
			InputStream error = connection.getErrorStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(error, "UTF-8"));
			String line;
			Log.e("org.test", "error message received from server:");
			while ((line = reader.readLine()) != null) {
				Log.e("org.test", line);
			}
			throw new IOException(String.format("invalid response code: %d (%s)", connection.getResponseCode(), connection.getResponseMessage()));
		}

	}

	public void setDoInput(boolean newValue) {
		connection.setDoInput(newValue);
	}

	public void setDoOutput(boolean newValue) {
		connection.setDoOutput(newValue);
	}

	public void addJson(String json) {
		this.json = json;
	}


}
