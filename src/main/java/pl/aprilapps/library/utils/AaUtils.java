package pl.aprilapps.library.utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.MediaStore.Images;
import android.util.Base64;
import android.util.Patterns;
import android.view.View;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class AaUtils {

    public static List<View> getCellsBelow(ListView listView, int position) {
        List<View> cells = new ArrayList<View>();

        for (int i = position + 1; i <= listView.getLastVisiblePosition(); i++) {
            cells.add(listView.getChildAt(i));
        }

        return cells;
    }

    public static long[] toPrimitives(Long... objects) {
        if (objects == null)
            return null;

        long[] primitives = new long[objects.length];
        for (int i = 0; i < objects.length; i++)
            primitives[i] = objects[i];

        return primitives;
    }

    public static String getStringFromFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line);
        }
        return total.toString();
    }

    public static String getTableCreateScript(Context context, String tableName) throws IOException {
        AssetManager am = context.getAssets();
        InputStream is = am.open("create_" + tableName + ".txt");
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line);
        }
        return total.toString();
    }

    public static Uri getImageUri(Context context, Bitmap inImage) {
        String path = Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static String formatTwoDigitsFloat(float number) {
        DecimalFormat df = new DecimalFormat("###,##");
        return df.format(number);
    }

    public static String formatTwoDigitsFloat(double number) {
        DecimalFormat df = new DecimalFormat("###,##");
        return df.format(number);
    }

    public static String formatOneDigitFloat(float number) {
        DecimalFormat df = new DecimalFormat("###,#");
        return df.format(number);
    }

    public static String formatOneDigitFloat(double number) {
        DecimalFormat df = new DecimalFormat("###,#");
        return df.format(number);
    }

    public static Bitmap bitmapFromUrl(String url) {
        try {
            URL req = new URL(url);
            Bitmap bitmap = BitmapFactory.decodeStream(req.openConnection().getInputStream());
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String[] getDeviceEmailAccounts(Context context) {
        final List<String> emails = new ArrayList<String>();
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        Account[] accounts = AccountManager.get(context).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                String email = account.name;
                if (!emails.contains(email))
                    emails.add(account.name);
            }
        }

        String[] emailsArray = new String[emails.size()];
        emailsArray = emails.toArray(emailsArray);
        return emailsArray;
    }

    public static String[] getFacebookKeyHashes(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            String[] hashes = new String[info.signatures.length];

            for (int i = 0; i < info.signatures.length; i++) {
                Signature signature = info.signatures[i];
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hash = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                hashes[i] = hash;
            }
            return hashes;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isAtLeastApiLevel(int apiLevel) {
        return (Build.VERSION.SDK_INT >= apiLevel);
    }

    public static boolean isAppInstalled(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static Intent getGooglePlayAppPageOpeningIntent(String packageName) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(String.format("market://details?id=%s", packageName)));
        return intent;
    }

    public static List<Contact> getContacts(Context context) {
        List<Contact> contacts = new ArrayList<>();

        Cursor contactsCursor = context.getContentResolver().query(
                ContactsContract.Contacts.CONTENT_URI,
                new String[]{ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME_PRIMARY,
                        ContactsContract.Contacts.PHOTO_THUMBNAIL_URI},
                null,
                null,
                ContactsContract.Contacts.DISPLAY_NAME_PRIMARY + " COLLATE LOCALIZED ASC");

        if (contactsCursor.moveToFirst()) {
            while (contactsCursor.moveToNext()) {
                long id = contactsCursor.getLong(contactsCursor.getColumnIndex(ContactsContract.Contacts._ID));
                Uri contactUri = Uri.parse(String.format("content://com.android.contacts/contacts/%d", id));
                Uri photoUri = null;
                String stringUri = contactsCursor.getString(contactsCursor.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI));
                if (stringUri != null) photoUri = Uri.parse(stringUri);

                Cursor emailCursor = context.getContentResolver().query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + id, null, null);

                List<String> emailList = new ArrayList<String>();
                while (emailCursor.moveToNext()) {
                    String email = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    if (!emailList.contains(email) && email.length() > 3)
                        emailList.add(email);
                }
                emailCursor.close();

                String name = contactsCursor.getString(contactsCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY));
                Contact contact = new Contact(id, contactUri, emailList, name, photoUri);

                if (emailList.size() > 0) {
                    contacts.add(contact);
                }
            }
        }
        contactsCursor.close();
        return contacts;
    }

}
