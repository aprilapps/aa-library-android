package pl.aprilapps.library.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;

public class AaAnimationUtils extends AaUtils {

    public interface OnDelayedAnimationListener {
        public void onDelayedAnimationStarted();
    }

    public static void expand(final View v) {
        v.measure(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
        final int targtetHeight = v.getMeasuredHeight();

        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1 ? LayoutParams.WRAP_CONTENT : (int) (targtetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targtetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void shrinkHeight(final View v, final float targetPercent) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = initialHeight - (int) (initialHeight * (interpolatedTime * targetPercent));
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void expandHeight(final View v, final int targetHeight) {
        v.measure(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

        v.getLayoutParams().height = 0;
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1 ? targetHeight : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void startViewAnimationWithDelay(Handler handler, final View view, final int animResId, long delayMilis, final OnDelayedAnimationListener listener) {
        view.setVisibility(View.INVISIBLE);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                view.setVisibility(View.VISIBLE);
                view.startAnimation(AnimationUtils.loadAnimation(view.getContext(), animResId));
                listener.onDelayedAnimationStarted();
            }
        }, delayMilis);
    }

    public static void startViewAnimationWithDelay(Handler handler, final View view, final int animResId, long delayMilis) {
        view.setVisibility(View.INVISIBLE);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                view.setVisibility(View.VISIBLE);
                view.startAnimation(AnimationUtils.loadAnimation(view.getContext(), animResId));
            }
        }, delayMilis);
    }

    public static void slideOutToLeftAnimation(Handler handler, final View viewToMove, long duration, float offset) {
        final TranslateAnimation animation = new TranslateAnimation(0f, (offset * -1f), 0f, 0f);
        animation.setDuration(duration);
        viewToMove.setAnimation(animation);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                viewToMove.setVisibility(View.INVISIBLE);
            }
        }, duration);
    }

    public static void slideOutToRightAnimation(Handler handler, final View viewToMove, long duration, float offset) {
        final TranslateAnimation animation = new TranslateAnimation(0f, offset, 0f, 0f);
        animation.setDuration(duration);
        viewToMove.setAnimation(animation);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                viewToMove.setVisibility(View.INVISIBLE);
            }
        }, duration);
    }

    public static void slideInFromRightAnimation(final View viewToMove, long duration, float offset) {
        viewToMove.setVisibility(View.VISIBLE);
        final TranslateAnimation animation = new TranslateAnimation(offset, 0f, 0f, 0f);
        animation.setDuration(duration);
        viewToMove.startAnimation(animation);
    }

    public static void slideInFromLeftAnimation(final View viewToMove, long duration, float offset) {
        viewToMove.setVisibility(View.VISIBLE);
        final TranslateAnimation animation = new TranslateAnimation((offset * -1f), 0f, 0f, 0f);
        animation.setDuration(duration);
        viewToMove.startAnimation(animation);
    }

    public static void crossfadeViews(final View viewToHide, final View viewToShow) {
        if (viewToShow == null) return;
        int animDuration = viewToShow.getContext().getResources().getInteger(android.R.integer.config_shortAnimTime);

        fadeIn(viewToShow, animDuration);
        fadeOut(viewToHide, animDuration);
    }

    public static void crossfadeViews(final View viewToHide, final View viewToShow, int animDuration) {
        fadeIn(viewToShow, animDuration);
        fadeOut(viewToHide, animDuration);
    }

    public static void fadeIn(final View view, int animDuration) {
        if (view == null) return;

        view.setAlpha(0f);
        view.setVisibility(View.VISIBLE);
        view.animate().alpha(1f).setDuration(animDuration).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.VISIBLE);
            }
        });
    }

    public static void fadeOut(final View view, int animDuration) {
        if (view == null) return;

        view.animate().alpha(0f).setDuration(animDuration).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.INVISIBLE);
            }
        });
    }

    public static void fadeIn(final View view) {
        if (view == null) return;

        int animDuration = view.getContext().getResources().getInteger(android.R.integer.config_shortAnimTime);

        view.setAlpha(0f);
        view.setVisibility(View.VISIBLE);
        view.animate().alpha(1f).setDuration(animDuration).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.VISIBLE);
            }
        });
    }

    public static void fadeOut(final View view) {
        if (view == null) return;

        int animDuration = view.getContext().getResources().getInteger(android.R.integer.config_shortAnimTime);

        view.animate().alpha(0f).setDuration(animDuration).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.INVISIBLE);
            }
        });
    }

    public static void backgroundColorTransition(View view, int previousColor, int newColor, int duration) {
        ObjectAnimator.ofObject(view, "backgroundColor", new ArgbEvaluator(), previousColor, newColor)
                .setDuration(duration)
                .start();
    }

}
