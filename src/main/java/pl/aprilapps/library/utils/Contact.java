package pl.aprilapps.library.utils;

import android.net.Uri;

import java.util.List;

/**
 * Created by Jacek Kwiecień on 26.11.2014.
 */
public class Contact {

    long id;
    Uri contactUri;
    List<String> emails;
    String name;
    Uri photo;

    public Contact(long id, Uri contactUri, List<String> emails, String name, Uri photo) {
        this.id = id;
        this.emails = emails;
        this.name = name;
        this.photo = photo;
        this.contactUri = contactUri;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Uri getPhoto() {
        return photo;
    }

    public void setPhoto(Uri photo) {
        this.photo = photo;
    }

    public Uri getContactUri() {
        return contactUri;
    }

    public void setContactUri(Uri contactUri) {
        this.contactUri = contactUri;
    }
}
