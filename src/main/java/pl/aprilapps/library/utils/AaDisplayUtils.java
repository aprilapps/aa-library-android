package pl.aprilapps.library.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;


/**
 * Created by jacek on 25.05.2014.
 */
public class AaDisplayUtils extends AaUtils {

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static int getScreenHeightInPixels(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (isAtLeastApiLevel(13)) {
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            return size.y;
        } else {
            Display display = wm.getDefaultDisplay();
            return display.getHeight(); // deprecated
        }
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static int getScreenWidthInPixels(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (isAtLeastApiLevel(13)) {
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            return size.x;
        } else {
            Display display = wm.getDefaultDisplay();
            return display.getWidth(); // deprecated
        }
    }

    public static int getScreenWidthInDp(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return pixelsToDp(context, displayMetrics.widthPixels);
    }

    public static int getScreenHeightInDp(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return pixelsToDp(context, displayMetrics.heightPixels);
    }

    public static int pixelsToDp(Context context, int px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (int) ((px / displayMetrics.density) + 0.5);
    }

    public static int dpToPixels(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (int) ((dp * displayMetrics.density) + 0.5);
    }

    public static void setError(TextView view, String errorText, Drawable errorDrawable) {
        errorDrawable.setBounds(new Rect(0, 0, errorDrawable.getIntrinsicWidth(), errorDrawable.getIntrinsicHeight()));
        view.setError(errorText, errorDrawable);
    }

    public static void setError(TextView view, int errorStringResId, int errorIconResId) {
        Context context = view.getContext();
        Drawable errorDrawable = context.getResources().getDrawable(errorIconResId);
        setError(view, context.getString(errorStringResId), errorDrawable);
    }

    public static void setError(TextView view, String errorText, int errorIconResId) {
        Context context = view.getContext();
        Drawable errorDrawable = context.getResources().getDrawable(errorIconResId);
        setError(view, errorText, errorDrawable);
    }

    public static void hideKeyboard(final View caller) {
        caller.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    InputMethodManager imm = (InputMethodManager) caller.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(caller.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                } catch (Exception e) {
                    Log.w(getClass().getSimpleName(), "Could not hide keyboard. " + e.getMessage());
                }
            }
        }, 300);
    }

    public static void showKeyboard(final View caller, Handler handler) {
        caller.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager imm = (InputMethodManager) caller.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(caller, InputMethodManager.SHOW_IMPLICIT);
            }
        }, 100);
    }
}
