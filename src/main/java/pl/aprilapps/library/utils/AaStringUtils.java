package pl.aprilapps.library.utils;

public class AaStringUtils extends AaUtils {

    public static boolean isEmailValid(String email) {
        boolean stricterFilter = true;
        String stricterFilterString = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        String laxString = ".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
        String emailRegex = stricterFilter ? stricterFilterString : laxString;
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(emailRegex);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public static boolean isAlphabetCharacter(String letter) {
        String pattern = "\\A\\p{L}+\\z";
        return letter.matches(pattern);
    }

    public static String trim(String string) {
        if (string == null)
            return null;
        string = string.trim();
        ;
        if (string.charAt(0) == (char) 160)
            string = string.substring(1);
        return string;
    }

    public static boolean contains(String matchedString, String query, boolean ignoreCase) {
        if (matchedString == null && query == null) {
            return true;
        } else if (matchedString == null || query == null) {
            return false;
        } else if (ignoreCase) {
            return matchedString.toLowerCase().contains(query.toLowerCase());
        } else {
            return matchedString.contains(query);
        }
    }

}
