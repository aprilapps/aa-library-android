package pl.aprilapps.library.ui.components;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.widget.ImageView;

import pl.aprilapps.library.utils.AaImageUtils;

public class AaNetworkImageView extends ImageView {

	private AsyncTask loader;
	private boolean cached;

	public AaNetworkImageView(Context context) {
		super(context);
	}

	public AaNetworkImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public AaNetworkImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public void loadImageFromUrl(String url, int defaultResId, boolean useCache) {
		if (!cached || !useCache) {
			setImageResource(defaultResId);
		}
		if (loader != null) {
			loader.cancel(true);
		}
		loader = new LoadImageAsyncTask().execute(url);
	}

	private class LoadImageAsyncTask extends AsyncTask<String, Void, Bitmap> {

		@Override
		protected Bitmap doInBackground(String... params) {
			String url = params[0];
			Bitmap bm = AaImageUtils.loadBitmapFromURL(url);
			return bm;
		}

		@Override
		protected void onPostExecute(Bitmap bm) {
			super.onPostExecute(bm);
			if (bm != null && getContext() != null) {
				setImageBitmap(bm);
				cached = true;
			}
			loader = null;
		}
	}

}