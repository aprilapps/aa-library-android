package pl.aprilapps.library.ui.fragments;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import pl.aprilapps.library.ui.AaApplication;
import pl.aprilapps.library.ui.activities.AaActivity;
import pl.aprilapps.library.ui.async.AaAsyncTask;
import pl.aprilapps.library.ui.async.AaAsyncTask.OnAaTaskListener;
import pl.aprilapps.library.ui.dialogs.AaDialogFragment;
import pl.aprilapps.library.utils.AaAnimationUtils;

public abstract class AaFragment extends Fragment implements OnAaTaskListener {

    protected AaAsyncTask<?, ?, ?> downloader;
    private View errorView;
    private View progressView;
    private int errorClickAction;
    private int shortAnimationDuration;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        shortAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);
    }

    public AaActivity getAaActivity() {
        return (AaActivity) getActivity();
    }

    public ActionBar getSupportActionBar() {
        return getAaActivity().getSupportActionBar();
    }

    public boolean isDownloading() {
        return (downloader != null);
    }

    public AaApplication getApp() {
        return (AaApplication) getActivity().getApplication();
    }

    public AaAsyncTask<?, ?, ?> getDownloader() {
        return downloader;
    }

    public void setDownloader(AaAsyncTask<?, ?, ?> downloader) {
        if (this.downloader != null)
            this.downloader.cancel(true);
        this.downloader = downloader;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void onDownloaderStarted() {
    }

    @Override
    public void onDownloaderFinished() {
        setDownloader(null);
    }

    @Override
    public void onDetach() {
        if (downloader != null && downloader.equals(getDownloader()))
            downloader.forceCancel();
        super.onDetach();
    }

    public boolean isTablet() {
        return getAaActivity().isTablet();
    }

    public LayoutInflater getLayoutInflater() {
        return getAaActivity().getLayoutInflater();
    }

    public SharedPreferences getPreferences() {
        return getApp().getPreferences();
    }

    public Editor getPreferencesEditor() {
        return getApp().getPreferencesEditor();
    }

    public int getScreenWidthInDp() {
        return getApp().getScreenWidthInDp();
    }

    public int getScreenHeightInDp() {
        return getApp().getScreenHeightInDp();
    }

    public int pixelsToDp(int pixels) {
        return getApp().pixelsToDp(pixels);
    }

    public int doToPixels(int dp) {
        return getApp().dpToPixels(dp);
    }

    public void showDialogFragment(AaDialogFragment dialog, Bundle args) {
        getAaActivity().showDialogFragment(dialog, args);
    }

    public void showToast(String text, int duration) {
        getApp().showToast(text, duration);
    }

    public View getProgressView() {
        return progressView;
    }

    public TextView getErrorLabel() {
        if (errorView == null) {
            return null;
        } else {
            return (TextView) errorView.findViewById(getErrorLabelResId());
        }
    }

    public void showErrorView(String message, View viewToHide) {
        if (getErrorLabel() != null) {
            getErrorLabel().setText(message);
        }

        if (errorView != null) {
            AaAnimationUtils.crossfadeViews(viewToHide, errorView, shortAnimationDuration);
        } else {
            Log.e(((Object) this).getClass().getSimpleName(), "Can't show error view, because it's null. Haven't you forgot to initErrorView() ?");
        }
    }

    public void showErrorView(View viewToHide) {
        showErrorView(null, viewToHide);
    }

    public void hideErrorView(View viewToShow) {
        if (errorView != null) {
            AaAnimationUtils.crossfadeViews(errorView, viewToShow, shortAnimationDuration);
        } else {
            Log.e(((Object) this).getClass().getSimpleName(), "Can't hide error view, because it's null. Haven't you forgot to initErrorView() ?");
        }
    }

    public void showProgressView(View viewToHide) {
        if (progressView != null) {
            AaAnimationUtils.crossfadeViews(viewToHide, progressView, shortAnimationDuration);
        } else {
            Log.e(((Object) this).getClass().getSimpleName(), "Can't show progress view, because it's null. Haven't you forgot to initProgressView() ?");
        }
    }

    public void hideProgressView(View viewToShow) {
        if (progressView != null) {
            AaAnimationUtils.crossfadeViews(progressView, viewToShow, shortAnimationDuration);
        } else {
            Log.e(((Object) this).getClass().getSimpleName(), "Can't hide progress view, because it's null. Haven't you forgot to initProgressView() ?");
        }
    }

    public void initProgressHandling(View fragmentView) {
        progressView = fragmentView.findViewById(getProgressViewResId());
        if (progressView != null) {
            progressView.setVisibility(View.INVISIBLE);
        }
    }

    public void initErrorHandling(View fragmentView) {
        errorView = fragmentView.findViewById(getErrorViewResId());
        if (errorView != null) {
            errorView.setVisibility(View.INVISIBLE);
        }
    }

    private View.OnClickListener progressLayoutClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onErrorViewTapped(errorClickAction);
        }
    };

    public void registerErrorClickListener(int errorClickAction) {
        this.errorClickAction = errorClickAction;
        if (errorView != null) {
            errorView.setOnClickListener(progressLayoutClickListener);
        }
    }

    public void unregisterProgressLayoutClickListener() {
        if (errorView != null) {
            errorView.setOnClickListener(null);
        }
    }

    public int getShortAnimationDuration() {
        return shortAnimationDuration;
    }

    public void onErrorViewTapped(int errorClickAction) {
        unregisterProgressLayoutClickListener();
    }

    public View getErrorView() {
        return errorView;
    }

    public abstract int getErrorViewResId();

    public abstract int getErrorLabelResId();

    public abstract int getProgressViewResId();

    /**
     * Injects fragment into activity. For animation always both enter and exit animations must be
     * provided. This method sets the fragment class name as a tag.
     *
     * @param fragment     Fragment to inject
     * @param viewHolderId Res id of container layout
     * @param replace      Whether fragment should be replaced, even if already exist
     */
    public void injectChildFragment(Fragment fragment, int viewHolderId, int enterAnimation, int exitAnimation, int popEnterAnimation, int popExitAnimation, boolean replace) {
        injectChildFragment(fragment, ((Object) fragment).getClass().getSimpleName(), viewHolderId, enterAnimation, exitAnimation, popEnterAnimation, popExitAnimation, replace);
    }

    public void injectChildFragmentWithoutAnimation(Fragment fragment, int containerId) {
        injectChildFragment(fragment, containerId, AaActivity.ANIM_NONE, AaActivity.ANIM_NONE, AaActivity.ANIM_NONE, AaActivity.ANIM_NONE, true);
    }

    /**
     * Injects fragment into activity. For animation always both enter and exit animations must be
     * provided.
     *
     * @param fragment     Fragment to inject
     * @param tag          Desired tag
     * @param viewHolderId Res id of container layout
     * @param replace      Whether fragment should be replaced, even if already exist
     */
    public void injectChildFragment(Fragment fragment, String tag, int viewHolderId, int enterAnimation, int exitAnimation, int popEnterAnimation, int popExitAnimation,
                                    boolean replace) {
        FragmentManager manager = getChildFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        if (enterAnimation != AaActivity.ANIM_NONE && exitAnimation != AaActivity.ANIM_NONE) {
            transaction.setCustomAnimations(enterAnimation, exitAnimation, popEnterAnimation, popExitAnimation);
        }

        if (fragment.isAdded() && !replace) {
            transaction.attach(fragment);
        } else if (replace) {
            transaction.replace(viewHolderId, fragment, tag);
        } else {
            transaction.add(viewHolderId, fragment, tag);
        }
        transaction.commit();
    }

}
