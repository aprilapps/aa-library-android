package pl.aprilapps.library.ui.activities;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import pl.aprilapps.library.ui.AaApplication;
import pl.aprilapps.library.ui.async.AaAsyncTask;
import pl.aprilapps.library.ui.async.AaAsyncTask.OnAaTaskListener;
import pl.aprilapps.library.ui.dialogs.AaDialogFragment;

public abstract class AaActivity extends ActionBarActivity implements OnAaTaskListener {

    public static final int ANIM_NONE = -1;
    public static final int NO_PROGRESS = -1;

    protected AaAsyncTask<?, ?, ?> downloader;
    private int progressClickAction;

    View progressLayout;

    @SuppressWarnings("rawtypes")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        supportRequestWindowFeature(Window.FEATURE_PROGRESS);
        super.onCreate(savedInstanceState);
        ((AaApplication) getApplication()).registerActivity(this);
        ((AaApplication) getApplication()).onActivityCreated(this, savedInstanceState);

        if (((AaApplication) getApplication()).isRemovedFromMemory())
            return;

        try {
            downloader = (AaAsyncTask<?, ?, ?>) getLastNonConfigurationInstance();
        } catch (ClassCastException e) {
            e.printStackTrace();
        }

        if (downloader != null)
            downloader.setListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getApp().onActivityStarted(this);
    }

    public AaAsyncTask<?, ?, ?> getDownloader() {
        return downloader;
    }

    public void setDownloader(AaAsyncTask<?, ?, ?> downloader) {
        // Log.v(AaApplication.TAG_LOG, "setDownloader");
        if (this.downloader != null)
            this.downloader.cancel(true);
        this.downloader = downloader;
    }

    @Override
    public void onDownloaderStarted() {
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        if (downloader != null)
            downloader.detach();
        return downloader;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (((AaApplication) getApplication()).isRemovedFromMemory()) return;
        setProgressBarIndeterminateVisibility(false);
        getApp().onActivityResumed(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getApp().onActivityPaused(this);
    }

    public void addFragment(Fragment fragment, int viewHolderId) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(viewHolderId, fragment, fragment.getClass().getSimpleName());
        transaction.commit();
    }

    /**
     * Injects fragment into activity. For animation always both enter and exit animations must be
     * provided. This method sets the fragment class name as a tag.
     *
     * @param fragment       Fragment to inject
     * @param viewHolderId   Res id of container layout
     * @param addToBackStack Whether fragment should be add to back stack
     * @param replace        Whether fragment should be replaced, even if already exist
     */
    public void injectFragment(Fragment fragment, int viewHolderId, int enterAnimation, int exitAnimation, int popEnterAnimation, int popExitAnimation, boolean addToBackStack,
                               boolean replace) {
        injectFragment(fragment, ((Object) fragment).getClass().getSimpleName(), viewHolderId, enterAnimation, exitAnimation, popEnterAnimation, popExitAnimation, addToBackStack,
                true);
    }

    /**
     * Injects fragment into activity. For animation always both enter and exit animations must be
     * provided.
     *
     * @param fragment       Fragment to inject
     * @param tag            Desired tag
     * @param viewHolderId   Res id of container layout
     * @param addToBackStack Whether fragment should be add to back stack
     * @param replace        Whether fragment should be replaced, even if already exist
     */
    public void injectFragment(Fragment fragment, String tag, int viewHolderId, int enterAnimation, int exitAnimation, int popEnterAnimation, int popExitAnimation,
                               boolean addToBackStack, boolean replace) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        if (enterAnimation != ANIM_NONE && exitAnimation != ANIM_NONE) {
            transaction.setCustomAnimations(enterAnimation, exitAnimation, popEnterAnimation, popExitAnimation);
        }

        if (fragment.isAdded() && !replace) {
            transaction.attach(fragment);
        } else if (replace) {
            transaction.replace(viewHolderId, fragment, tag);
            if (addToBackStack) {
                transaction.addToBackStack(tag);
            }
        } else {
            transaction.add(viewHolderId, fragment, tag);
            if (addToBackStack) {
                transaction.addToBackStack(tag);
            }
        }
        transaction.commit();
    }

    @Override
    public void onDownloaderFinished() {
        downloader = null;
    }

    public AaApplication getApp() {
        return (AaApplication) getApplication();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                int backStackCount = getFragmentManager().getBackStackEntryCount();
                if (backStackCount <= 1)
                    onHomePressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onHomePressed() {
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        getApp().onActivityStopped(this);
    }

    @Override
    protected void onDestroy() {
        ((AaApplication) getApplication()).unregisterActivity(this);
        if (isDownloading())
            getDownloader().cancel(true);
        super.onDestroy();
        getApp().onActivityDestroyed(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getApp().onActivitySaveInstanceState(this, outState);
    }

    public boolean isDownloading() {
        // Log.v(AaApplication.TAG_LOG, "isDownloading");
        return (getDownloader() != null);
    }

    @SuppressLint("NewApi")
    public boolean isTablet() {
        if (Build.VERSION.SDK_INT < 11)
            return false;

        Configuration config = getResources().getConfiguration();
        try {
            return (config.smallestScreenWidthDp >= 600);
        } catch (Exception e) {
            return false;
        }
    }

    public SharedPreferences getPreferences() {
        return ((AaApplication) getApplication()).getPreferences();
    }

    public Editor getPreferencesEditor() {
        return ((AaApplication) getApplication()).getPreferencesEditor();
    }

    public int getScreenWidthInDp() {
        return ((AaApplication) getApplication()).getScreenWidthInDp();
    }

    public int getScreenHeightInDp() {
        return ((AaApplication) getApplication()).getScreenHeightInDp();
    }

    public void showDialogFragment(AaDialogFragment dialog, Bundle args) {
        if (args != null)
            dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), ((Object) dialog).getClass().getSimpleName());
    }

    public void showToast(String text, int duration) {
        ((AaApplication) getApplication()).showToast(text, duration);
    }

    public void showProgressLayout(String message, View... viewsToHide) {
        if (progressLayout != null) {
            progressLayout.setVisibility(View.VISIBLE);

            TextView label = (TextView) progressLayout.findViewById(getProgressLabelResId());
            if (label != null && message != null) {
                label.setText(message);
            }

            for (View view : viewsToHide) {
                view.setVisibility(View.INVISIBLE);
            }
        }
    }

    public void showProgressLayout(View... viewsToHide) {
        showProgressLayout(null, viewsToHide);
    }

    public void hideProgressLayout(View... viewsToShow) {
        if (progressLayout != null) {
            progressLayout.setVisibility(View.INVISIBLE);

            for (View view : viewsToShow) {
                view.setVisibility(View.VISIBLE);
            }
        }
    }

    public void initProgressLayout() {
        progressLayout = findViewById(getProgressViewResId());
        if (progressLayout != null) {
            progressLayout.setVisibility(View.INVISIBLE);
        }
    }

    private View.OnClickListener progressLayoutClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onProgressLayoutTapped(progressClickAction);
        }
    };

    public void registerProgressLayoutClickListener(int progressClickAction) {
        this.progressClickAction = progressClickAction;
        if (progressLayout != null) {
            progressLayout.setOnClickListener(progressLayoutClickListener);
        }
    }

    public void unregisterProgressLayoutClickListener() {
        if (progressLayout != null) {
            progressLayout.setOnClickListener(null);
        }
    }

    public void onProgressLayoutTapped(int progressClickAction) {

    }

    public View getProgressLayout() {
        return progressLayout;
    }

    public int getProgressViewResId() {
        return NO_PROGRESS;
    }

    public int getProgressLabelResId() {
        return NO_PROGRESS;
    }

}
