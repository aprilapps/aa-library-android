package pl.aprilapps.library.ui.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;

import pl.aprilapps.library.ui.AaApplication;

public class AaSplashScreenActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getApp().registerActivity(this);
		super.onCreate(savedInstanceState);
	}

	public LayoutInflater getInflater() {
		return (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
	}

	@Override
	protected void onDestroy() {
		getApp().unregisterActivity(this);
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	public AaApplication getApp() {
		return (AaApplication) getApplication();
	}
}
