package pl.aprilapps.library.ui.dialogs;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v4.app.DialogFragment;

import pl.aprilapps.library.ui.AaApplication;
import pl.aprilapps.library.ui.activities.AaActivity;

public class AaDialogFragment extends DialogFragment {

    public AaActivity getAaActivity() {
        return (AaActivity) getActivity();
    }

    public AaApplication getApp() {
        return getAaActivity().getApp();
    }

    public SharedPreferences getPreferences() {
        return getApp().getPreferences();
    }

    public Editor getPreferencesEditor() {
        return getApp().getPreferencesEditor();
    }

    public int getScreenWidthInDp() {
        return getApp().getScreenWidthInDp();
    }

    public int getScreenHeightInDp() {
        return getApp().getScreenHeightInDp();
    }

    public void showToast(String text, int duration) {
        getApp().showToast(text, duration);
    }

}
