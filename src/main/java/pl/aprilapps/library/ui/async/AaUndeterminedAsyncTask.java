package pl.aprilapps.library.ui.async;

import android.util.Log;

import pl.aprilapps.library.ui.dialogs.AaDialogFragment;

public abstract class AaUndeterminedAsyncTask<Params, Progress, Result> extends AaAsyncTask<Params, Progress, Result> {

	protected AaDialogFragment progressDialog;

	public AaUndeterminedAsyncTask(OnAaTaskListener listener) {
		super(listener);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	protected Result doInBackground(Params... params) {
		return null;
	}

	;

	protected void onPostExecute(Result result) {
		super.onPostExecute(result);
		setFinished(true);
		try {
			if (progressDialog != null)
				progressDialog.dismiss();
		} catch (Exception e) {
            Log.w("AA", "Could not dismiss progress dialog");
        }
	}

	public AaDialogFragment getProgressDialog() {
		return progressDialog;
	}

	public void setProgressDialog(AaDialogFragment progressDialog) {
		this.progressDialog = progressDialog;
	}

	;

	@Override
	public void forceCancel() {
		super.forceCancel();
		try {
			if (progressDialog != null)
				progressDialog.dismiss();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
