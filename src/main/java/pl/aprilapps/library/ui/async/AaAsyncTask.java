package pl.aprilapps.library.ui.async;

import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import pl.aprilapps.library.ui.AaApplication;
import pl.aprilapps.library.ui.activities.AaActivity;

public abstract class AaAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    private boolean finished;
    private boolean activityListener;
    private OnAaTaskListener listener;

    public AaAsyncTask(OnAaTaskListener listener) {
        this.listener = listener;
        if (listener instanceof AaActivity)
            activityListener = true;
        else
            activityListener = false;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.v(AaApplication.TAG_LOG, "Executing task: " + this.getClass().getSimpleName());
        try {
            listener.onDownloaderStarted();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected Result doInBackground(Params... params) {
        return null;
    }

    protected void onPostExecute(Result result) {
        super.onPostExecute(result);
        try {
            if (isCancelled())
                return;
            listener.onDownloaderFinished();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void detach() {
        onDetach();
    }

    public void onDetach() {
        this.listener = null;
    }

    public OnAaTaskListener getBaseListener() {
        return listener;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public OnAaTaskListener getListener() {
        return listener;
    }

    public void setListener(OnAaTaskListener listener) {
        this.listener = listener;
    }

    protected AaActivity getActivity() {
        if (activityListener)
            return (AaActivity) getListener();
        else {
            Fragment fragment = (Fragment) getListener();
            return (AaActivity) fragment.getActivity();
        }
    }

    public void forceCancel() {
        this.cancel(true);
    }

    public interface OnAaTaskListener {
        public void onDownloaderStarted();

        public void onDownloaderFinished();
    }

}
