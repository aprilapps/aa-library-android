package pl.aprilapps.library.ui.async;

public abstract class AaUndeterminedActionBarAsyncTask<Params, Progress, Result> extends AaAsyncTask<Params, Progress, Result> {

	public AaUndeterminedActionBarAsyncTask(OnAaTaskListener listener) {
		super(listener);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		getActivity().setProgressBarIndeterminateVisibility(true);
	}

	protected Result doInBackground(Params... params) {
		return null;
	}

	;

	protected void onPostExecute(Result result) {
		super.onPostExecute(result);
		getActivity().setProgressBarIndeterminateVisibility(false);
	}

	;

	@Override
	public void forceCancel() {
		super.forceCancel();
		getActivity().setProgressBarIndeterminateVisibility(false);
	}

}
