# APRILAPPS ANDROID LIBRARY #


## What is this project for? ##

It's a set of useful methods and classes I wrote over several years of Android development. I find them very useful.

## Instalation ##

### Android studio ###
This is Android studio module, so just clone repo, import module and ad it to gradle dependencies

### Eclipse ###
For eclipse you might wanna just coppy paste classes into your project

## Usage ##

####For full experience you might wanna extend your base UI classes with AA classes as following:####

* Instead of extending Application extend **AaApplication**
* Instead of extending Activity extend **AaActivity**
* Instead of extending Fragment extend **AaFragment**
* Instead of extending DialogFragment extend **AaDialogFragment**
* Instead of extending AsyncTask extend **AaSyncTask**

###AaActivity, AaFragment convenience methods###

* You can eaisly show dialog fragment and pass arguments to is using method:

```
#!java

public void showDialogFragment(AaDialogFragment dialog, Bundle args) ;
```

* Easy access to your custom Application:

```
#!java

public AaApplication getApp();
```
You can overwrite it in your custom Activity or Fragment to return your custom Application class

* Easy access to SharedPreferences and the Editor objects:

```
#!java

public SharedPreferences getPreferences();
public Editor getPreferencesEditor();
```
* Showing default Android toast eaisly:

```
#!java

public void showToast(String text, int duration)
```

###AaApplication convenience methods###
* Geting version code


```
#!java

public int getVersionCode();
```

* Checking if the device has the hardware keyboard


```
#!java

public boolean hasDeviceHardwareKeyboard();
```

* Converting pixels to dp and in reverse


```
#!java

public int pixelsToDp(int px);
public int dpToPixels(int dp);
```

* Checking if the app is in foreground (requires GET_TASKS permission)


```
#!java

public boolean isAppInForeground();
```

* Checking if the device has external memory


```
#!java

isExternalStorageAvailable();
```

###Killing app on demand###
There is a method for this inside AaPlication:

```
#!java

public void killApp();
```
It will only work if all your activities inherits directly or indirectly from AaActivity

###ImageView with image from url###
You can use AaNetworkImageView to eaisly load images from urls right into ImageView with simple method:

```
#!java

public void loadImageFromUrl(String url, int defaultResId, boolean useCache);
```

###Async tasks###
You can have a single async task for each fragment or activity, that should be only executed only one at the time. Let's say the task that is loading data for your view. You will wan't this task to be canceled if the new one is executed and to let it retain references to your Fragment or Activity even if the screen was rotated. In this case your fragment or Activity simply use:


```
#!java

        SomeAsyncTask task = new SomeAsyncTask(this);
        setDownloader(task);
        task.execute();
```

Remember to inherit your async task from AaAsyncTask. It should always take a listener interface as an argument, so the task know what to adress in onPostExecute() when it finishes. Here is sample implementation of custom AsyncTask:


```
#!java

public class GetUserNamesTask extends AaAsyncTask<Void, Void, List<String>> {

	public GetUserNamesTask(IGetUserNamesTask listener) {
		super(listener);
	}

	@Override
	protected List<String> doInBackground(Void... params) {
		List<String> list = new ArrayList<String>();
		//do your background operations here, for example request some data from web service
        return list;
	}

	@Override
	protected void onPostExecute(List<String> names) {
		if (!isCancelled()) {
			getListener().onUserNamesLoaded(names);
			super.onPostExecute(names);
		}
	}

	@Override
	public IGetUserNamesTask getListener() {
		return (IGetUserNamesTask) super.getListener();
	}

	public interface IGetUserNamesTask extends IAaTask {
		public void onUserNamesLoaded(List<String> purchases);
	}

}
```


It will process the data you need and push it to the listener (in your case it should be Fragment or Activity).

###Progress and error handling###
The library also includes some modern standards UI progress/error handling. Let's say you executed the task, and it returned error to your Fragment or the Activity. You might wanna inform the user that something wen't wrong. Back in the days you would just create some Dialog and throw it to the user, but it's cleaner if you display some information as a part of your current screen. To do that you will need to have 3 things done:

####1. Create custom view for your error handling. This is the example:####


```
#!xml

<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:id="@+id/progress_layout"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:clickable="true">


    <ImageView
        android:id="@+id/cover_logo"
        android:layout_width="300dp"
        android:layout_height="300dp"
        android:src="@drawable/logo_wide"
        android:layout_alignParentTop="true"
        android:layout_centerHorizontal="true" />

    <TextView
        android:id="@+id/progress_label"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:paddingLeft="30dp"
        android:paddingRight="30dp"
        android:ellipsize="end"
        android:gravity="center"
        android:maxLines="8"
        android:text="@string/loading_message"
        android:textAllCaps="true"
        android:layout_below="@id/cover_logo"/>

    <ProgressBar
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:id="@+id/progress_bar"
        android:layout_centerHorizontal="true"
        android:layout_marginTop="10dp"
        android:layout_below="@id/progress_label" />

</RelativeLayout>
```

This view should always contain TextView with id progress_label. Also its root should has an id: progress_layout. Rest of the elements you might wanna put there is up to you. Those 2 are obligatory.

####2. Include newly created layout in any Fragment or Activity you want. Don't set the include id.####
####3. In onCreate() for Activity and onCreateView() for Fragments execute method initProgressLayout()####

Now you have several posibilities opened.

By default your progress view is hidden. It's up to you how you position it on your view. I'd recomend RelativeLayout however.

####Showing progress view####

```
#!java

public void showProgressLayout(String message, View... viewsToHide);
public void showProgressLayout(View... viewsToHide);
```

Those method will show your progress view, and hide other views if you provide them. You might wanna hide your content and show progress layout instead. If you specify message then the progress_label will get this text instead the one you specified in xml.

####Showing errors####
You show the error exact the same way as described above. However you might also want to implement click posibility for your error view. Let's say, that you put in your progress_label such information "Error loading data. Tap this info to try again". So now on the error message tap you wan't to execute some code. You do this like this:


```
#!java

//error was returned here, lets warn the user
final int MY_ERROR_CODE = 1;
public void showProgressLayout("Error loading data. Tap this info to try again", myContentView);
//if you have ProgressBar you will want to hide it.
getProgressLayout().findViewById(R.id.progress_bar).setVisibility(View.INVISIBLE);
//register click listener with the error code (you might wanna handle several errors in the same fragment, that what is the error code is for)
registerProgressLayoutClickListener(MY_ERROR_CODE);
```

This is it, now the progress layout is clickable. To handle the click you just have to override method:


```
#!java

    @Override
    public void onProgressLayoutTapped(int progressClickAction) {
        super.onProgressLayoutTapped(progressClickAction);
        if (progressClickAction == MY_ERROR_CODE) {
            //now you know what error was tapped, and you can execute your code
        }
    }
```


Remember that, if you start loading data again after tapping the error, you should unregister click listener so the user cannot click on it again and execute the loading again. Just do this before loading starts:


```
#!java

unregisterProgressLayoutClickListener();
```
